const { src, dest } = require('gulp')
const autoprefixer = require('gulp-autoprefixer')
const stylus = require('gulp-stylus')
const sourcemaps = require('gulp-sourcemaps')
const rename = require('gulp-rename')
const gulpif = require('gulp-if')
const toaster = require('./toaster')

const isDevelopment = process.env.NODE_ENV !== 'production'

// markup
function buildStyles (cb) {
  return src('src/styles/index.styl')
    .pipe(toaster('Stylus', cb))
    .pipe(gulpif(isDevelopment, sourcemaps.init()))
    .pipe(stylus({
      'include css': true
    }))
    .pipe(rename({ basename: 'styles' }))
    .pipe(autoprefixer('last 4 version', '>= ie 10'))
    .pipe(gulpif(isDevelopment, sourcemaps.write('./')))
    .pipe(dest('dist/css'))
}

module.exports = { buildStyles }
