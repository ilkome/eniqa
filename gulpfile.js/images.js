const { dest, src } = require('gulp')
const imagemin = require('gulp-imagemin')
const imageminPngquant = require('imagemin-pngquant')
const toaster = require('./toaster')

function optimizeImages (cb) {
  return src('static/**/*.+(jpg|png|gif|svg)')
    .pipe(toaster('Images', cb))
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [
        { removeViewBox: false },
        { cleanupIDs: true }
      ],
      use: [imageminPngquant()]
    }))
    .pipe(dest('dist'))
}

module.exports = { optimizeImages }
