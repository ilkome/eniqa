const del = require('del')

// clean
function cleanDistFolder () {
  return del('dist/**/*')
};

module.exports = { cleanDistFolder }
