/*
  ilkome frontend
  Version 5.0.0

  Ilya Komichev
  https://ilko.me
  https://github.com/ilkome/frontend
*/
const { series, parallel, watch } = require('gulp')
const { runServer, reloadServer } = require('./server')
const { cleanDistFolder } = require('./clean')
const { copyStaticFiles } = require('./static')
const { buildMarkup } = require('./markup')
const { buildStyles } = require('./styles')
const { optimizeImages } = require('./images')
const { upload, uploadMin } = require('./upload')

// watchers
function runWatchers (cb) {
  watch([
    'src/pages/**/*.pug',
    'src/layout/*.pug',
    'src/atoms/**/*.pug'
  ], series(buildMarkup)).on('change', reloadServer)
  watch(['src/styles/**/*.styl', 'src/pages/**/*.styl'], series(buildStyles))
  watch(['static/**/*', '!static/**/*.+(jpg|png|gif|svg)'], series(copyStaticFiles))
  watch('static/**/*.+(jpg|png|gif|svg)', series(optimizeImages))
  cb()
}

// development task
exports.default = series(
  cleanDistFolder,
  parallel(copyStaticFiles),
  parallel(buildStyles, buildMarkup, runServer, optimizeImages),
  runWatchers
)

// build task
exports.build = series(
  cleanDistFolder,
  parallel(copyStaticFiles),
  parallel(buildStyles, buildMarkup, optimizeImages)
)

// upload task
exports.upload = upload
exports.uploadMin = uploadMin

// clean task
exports.clean = cleanDistFolder
