const browserSync = require('browser-sync')
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const webpackConfig = require('../webpack.config.dev')

const webpackBundler = webpack(webpackConfig)

function runServer (cb) {
  browserSync.init({
    server: { baseDir: 'dist' },
    middleware: [
      webpackDevMiddleware(webpackBundler, {
        ...webpackConfig,
        publicPath: webpackConfig.output.publicPath
      }),
      webpackHotMiddleware(webpackBundler)
    ],
    open: false,
    logFileChanges: false,
    notify: false,
    online: true,
    files: [
      'dist/css/*.css',
      'dist/css/*.html'
    ]
  })
  cb()
}

// reload server
function reloadServer () {
  browserSync.reload()
}

module.exports = {
  runServer,
  reloadServer
}
