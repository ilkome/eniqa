# ilkome frontend template
Powerful frontend boilerplate. HTML5 & CSS3, Pug, Stylus, ES6, Autoprefixer, Browsersync, Gulp

## Features

### HTML
Template engine using Pug (ex pug). Pug it's preprocessor HTML.

### CSS
- Stylus compilation including source maps
- Components system with Stylus. Stylus it's preprocessor CSS. It's also can be SASS, LESS
- Helpful mixins
- Minify styles
- Remove unused CSS styles. For example from CSS libraries like bootstrap
- Autoprefixer. Parse CSS and add vendor prefixes to rules

### JavaScript
- Support ES6
- Compiles ES6 to ES5
- Minify

### Productivity
- Browsersync. It's runs local server and reload the browsers across all your devices when you make changes in your application folder files
- Hot reload for images. Minify images: svg, png, jpg, gif
- Upolad builded files on the server


# Setup

### Project setup
``` bash
# clone the repo
$ git clone

# go into app's directory
$ cd

# install app's dependencies
$ npm install
```

### Setup upload task

Create `ftp.config.js` in root folder of project.
Write your FTP config for upload files to the server.

```JavaScript
module.exports = {
  host: '',
  user: '',
  password: '',
  dest: '/'
}
```

## Upload to server
You can add your FTP config in app's directory `/src/ftp.config.js`

``` bash
# upload all files from dist folder
$ npm run upload

# upload only css, js, html files from dist folder
$ npm run upload-min
```

# Usage

Compile App and watch files for changes.
- `npm run dev` development
- `npm run build` make build
- `npm run upload` make build and upload files to server


# Tasks
This command will give you a list of all tasks available.
```bash
gulp --tasks
```

- `clean` clean build folder.
- `markup` compile pug files.
- `html` prettify compiled html.
- `images` minify images.
- `assets` copy everything from assets folder. Used for favicons, fonts, css.
- `styles` compiles styles.
- `css` analyze HTML files and clean unused CSS styles. Ignore styles with prefix `.js-`. Add vendor prefixes.
- `upload` upload build folder on the server.


# Structure of main folders and files
    .
    ├── app                                 # Application folder
    │   ├── atoms                           # Atoms
    │   │   └── atom-name                   # Atom's example. It's a component pug + Stylus + Images
    │   │       ├── img                     # Atom's images
    │   │       ├── atom-name.pug          # Atom's markdown
    │   │       └── atom-name.styl          # Atom's styles.
    │   │
    │   ├── js                              # JavaScript
    │   │   ├── index.js                    # Entry
    │   │   └── libs                        # Libraries
    │   │
    │   ├── layout                          # Layout files
    │   │   ├── head.pug                   # Head
    │   │   ├── layout.pug                 # Layout
    │   │   ├── scripts.pug                # Scripts
    │   │   └── styles.pug                 # Styles
    │   │
    │   ├── pages                           # Pages of application
    │   │   │── index.pug                  # Index
    │   │   └── contacts.pug               # Contacts
    │   │
    │   ├── assets                          # Assets
    │   │   │── css                         # CSS
    │   │   │── fonst                       # Fonts
    │   │   │── icons                       # Icons
    │   │   └── img                         # Images files. Will minify
    │   │
    │   └── stylus                          # Stylus
    │       │── base                        # Base styles
    │       │── helpers                     # Helpers mixins
    │       │── index.styl
    │       │── layout.styl
    │       └── variables.styl
    │
    ├── dist                                # Build project to this folder
    │
    ├── gulpfle.js                          # Gulp config and tasks
    │   └── index.js                        # Gulp main tasks and watchers
    │
    │
    ├── ftp.config.js                       # Config for FTP connection
    ├── README.md
    └── webpack.config.js
