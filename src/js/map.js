/* global google */
const styles = [{ featureType: 'all', elementType: 'labels.text.fill', stylers: [{ saturation: 36 }, { color: '#333333' }, { lightness: 40 }] }, { featureType: 'all', elementType: 'labels.text.stroke', stylers: [{ visibility: 'on' }, { color: '#ffffff' }, { lightness: 16 }] }, { featureType: 'all', elementType: 'labels.icon', stylers: [{ visibility: 'off' }] }, { featureType: 'administrative', elementType: 'geometry.fill', stylers: [{ color: '#fefefe' }, { lightness: 20 }] }, { featureType: 'administrative', elementType: 'geometry.stroke', stylers: [{ color: '#fefefe' }, { lightness: 17 }, { weight: 1.2 }] }, { featureType: 'landscape', elementType: 'geometry', stylers: [{ color: '#f5f5f5' }, { lightness: 20 }] }, { featureType: 'poi', elementType: 'geometry', stylers: [{ color: '#f5f5f5' }, { lightness: 21 }] }, { featureType: 'poi.park', elementType: 'geometry', stylers: [{ color: '#dedede' }, { lightness: 21 }] }, { featureType: 'road.highway', elementType: 'geometry.fill', stylers: [{ color: '#ffffff' }, { lightness: 17 }] }, { featureType: 'road.highway', elementType: 'geometry.stroke', stylers: [{ color: '#ffffff' }, { lightness: 29 }, { weight: 0.2 }] }, { featureType: 'road.arterial', elementType: 'geometry', stylers: [{ color: '#ffffff' }, { lightness: 18 }] }, { featureType: 'road.local', elementType: 'geometry', stylers: [{ color: '#ffffff' }, { lightness: 16 }] }, { featureType: 'transit', elementType: 'geometry', stylers: [{ color: '#f2f2f2' }, { lightness: 19 }] }, { featureType: 'water', elementType: 'geometry', stylers: [{ color: '#e9e9e9' }, { lightness: 17 }] }]

const locations = [
  ['Location1', 55.761244, 37.618423, '#address1'],
  ['Location2', 55.741244, 37.568423, '#address2'],
  ['Location3', 55.721244, 37.628423, '#address3'],
  ['Location4', 59.938480, 30.312480, '#address4']
]

function setNewLocation (map, newLat, newLng) {
  map.setZoom(11)
  map.setCenter({
    lat: newLat,
    lng: newLng
  })
}

function setMarkers (map) {
  const markerImage = {
    url: './img/map-marker.png',
    size: new google.maps.Size(47, 51)
  }

  for (let i = 0; i < locations.length; i++) {
    const place = locations[i]
    const marker = new google.maps.Marker({
      map,
      position: {
        lat: place[1],
        lng: place[2]
      },
      icon: markerImage,
      title: place[0],
      url: place[3]
    })

    google.maps.event.addListener(marker, 'click', () => {
      const element = document.querySelector(marker.url)
      element.scrollIntoView({
        behavior: 'smooth'
      })
    })
  }
}

function initMap () {
  const map = new google.maps.Map(document.querySelector('.js-map'), {
    zoom: 11,
    center: { lat: 55.751244, lng: 37.618423 },
    mapTypeControl: false,
    styles
  })
  setMarkers(map)
  return map
}

// show map
const mapBlock = document.querySelector('.js-map')
if (mapBlock) {
  const map = initMap()
  const mskLocation = document.querySelector('.js-msk')
  const piterLocation = document.querySelector('.js-piter')
  if (mskLocation) {
    mskLocation.addEventListener('click', () => {
      setNewLocation(map, 55.751244, 37.618423)
    })
  }
  if (piterLocation) {
    piterLocation.addEventListener('click', () => {
      setNewLocation(map, 59.938480, 30.312480)
    })
  }
}
