import Swiper from 'swiper/dist/js/swiper.min' // swiper work's only like this in iOS10
import AOS from 'aos'
import cssVars from 'css-vars-ponyfill'

const iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream

// slider settings
// -----------------------------------------------
const baseSliderOption = {
  loop: true,
  navigation: {
    nextEl: '.slider__next',
    prevEl: '.slider__prev',
    disabledClass: '_disable'
  }
}

const doorSliderNavigation = {
  pagination: {
    el: '.slider__pagination',
    type: 'bullets',
    clickable: true,
    bulletClass: 'slider__bullet',
    bulletActiveClass: '_active',
    renderBullet: (index, className) => `<div class="${className} index-${index + 1}"></div>`
  }
}

const galleySlider = {
  pagination: {
    el: '.slider__pagination',
    type: 'fraction'
  }
}

// initDropdownMenu
// -----------------------------------------------
function initDropdownMenu() {
  const parent = document.querySelectorAll('.js-dropdown-filter')
  if (parent) {
    for (let i = 0; i < parent.length; i++) {
      const button = parent[i].querySelector('.dropdownBlock__title')
      const box = parent[i].querySelector('.dropdownBlock__content')
      const plus = parent[i].querySelector('.plus')
      let expanded = false

      const height = box.clientHeight
      box.style.height = 0

      button.addEventListener('click', () => {
        if (expanded) {
          plus.innerHTML = '+'
          box.style.height = 0
          expanded = false
          box.classList.remove('_show')
        } else {
          plus.innerHTML = '-'
          box.classList.add('_show')
          box.style.height = `${height}px`
          expanded = true
        }
      })
    }
  }
}

// modalInit
// -----------------------------------------------
function modalInit({ opener, modal, slider, lockOniOs = true }) {
  const mainMenu = document.querySelectorAll(opener)
  const body = document.querySelector('.js-body')

  for (let i = 0; i < mainMenu.length; i++) {
    const modalContainer = document.querySelector(modal)
    if (mainMenu[i] && modalContainer) {
      const closeButtons = modalContainer.querySelectorAll('.js-closeModal')

      // open
      mainMenu[i].addEventListener('click', () => {
        if (iOS && lockOniOs) {
          // because ios jump page to top first
          body.classList.add('_iosLock')
          setTimeout(() => {
            body.classList.add('_overflow')
            modalContainer.classList.add('_show')
          }, 10)
        } else {
          body.classList.add('_overflow')
          modalContainer.classList.add('_show')
        }

        if (slider) slider.update()
      })

      // close
      for (let i = 0; i < closeButtons.length; i++) {
        closeButtons[i].addEventListener('click', () => {
          body.classList.remove('_overflow', '_iosLock')
          modalContainer.classList.remove('_show')
        })
      }
    }
  }
}

// documentReady
// -----------------------------------------------
function documentReady() {
  AOS.init({ once: true, disable: false })

  initDropdownMenu()

  // sliders
  const indexSlider = new Swiper('.js-index-slider', baseSliderOption)
  const gallerySlider = new Swiper('.js-gallery-slider', { ...baseSliderOption, ...galleySlider })
  const doorSlider = new Swiper('.js-door-slider', { ...baseSliderOption, ...doorSliderNavigation })

  // modal menus
  modalInit({
    opener: '.js-handleClickMenu',
    modal: '.js-modalMenu',
    lockOniOs: false
  })
  modalInit({
    opener: '.js-showModalFilters',
    modal: '.js-modalFilters'
  })
  modalInit({
    opener: '.js-showModalGallery',
    modal: '.js-modalGallery',
    slider: gallerySlider
  })
  modalInit({
    opener: '.js-showModalContacts',
    modal: '.js-modalContacts'
  })

  // ie11 css variables
  cssVars()
}


// documentLoad
// -----------------------------------------------
function documentLoad() {
  const app = document.querySelector('.js-app')
  app.classList.add('_loaded')
}

// run app
// -----------------------------------------------
document.addEventListener('DOMContentLoaded', documentReady)
window.addEventListener('load', documentLoad)
